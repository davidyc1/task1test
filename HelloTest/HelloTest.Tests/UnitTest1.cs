using NUnit.Framework;

namespace HelloTest.Tests
{
    public class Tests
    {

        [TestCase("Hello ", "")]
        [TestCase("Hello Sergey", "Sergey")]

        public void GetSumOneTests(string name, string expected)
        {
            Assert.AreEqual(expected, Hellower.SayHello(name));
        }
    }
}